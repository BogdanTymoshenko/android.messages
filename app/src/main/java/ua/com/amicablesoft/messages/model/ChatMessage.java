package ua.com.amicablesoft.messages.model;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import ua.com.amicablesoft.messages.storage.MessagesTable;
import ua.com.amicablesoft.messages.utils.UtilsCursor;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
public class ChatMessage {

    public enum Type {
        FROM_ME(100),
        FROM_CONTACT(200);

        public final int value;

        @Override
        public String toString() {
            return "Type{" +
                    "value=" + value +
                    '}';
        }

        public static Type fromValue(int value) {
            for (Type type : values()) {
                if (type.value == value) {
                    return type;
                }
            }

            throw new IllegalArgumentException("Unknown value "+value);
        }


        private Type(int value) {
            this.value = value;
        }
    }


    private DateTime date;
    private String text;
    private Type type;


    public ChatMessage(String text, Type type) {
        this.date = new DateTime(DateTimeZone.getDefault());
        this.text = text;
        this.type = type;
    }

    private ChatMessage() {
    }


    public DateTime getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public Type getType() {
        return type;
    }


    public static ChatMessage fromCursor(@NonNull Cursor c) {
        if (c == null)
            throw new IllegalArgumentException("Cursor is null pointer");

        ChatMessage message = new ChatMessage();

        message.date = UtilsCursor.readDate(c, MessagesTable.COLUMN_DATE);
        message.text = UtilsCursor.readString(c, MessagesTable.COLUMN_MESSAGE_TEXT);
        message.type = Type.fromValue(UtilsCursor.readInt(c, MessagesTable.COLUMN_TYPE));

        return message;
    }
}
