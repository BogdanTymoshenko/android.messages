package ua.com.amicablesoft.messages.service;

/**
* Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
*/
public interface MessagesUpdateListener {
    public void onMessagesUpdated();
}
