package ua.com.amicablesoft.messages.utils;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
public final class UtilsCursor {

    public static DateTime readDate(@NonNull Cursor c, @NonNull String columnName) {
        int columnIdx = c.getColumnIndexOrThrow(columnName);
        long timestamp = c.getLong(columnIdx);

        return new DateTime(timestamp, DateTimeZone.getDefault());
    }

    public static String readString(@NonNull Cursor c, @NonNull String columnName) {
        int columnIdx = c.getColumnIndexOrThrow(columnName);
        return c.getString(columnIdx);
    }

    public static int readInt(@NonNull Cursor c, @NonNull String columnName) {
        int columnIdx = c.getColumnIndexOrThrow(columnName);
        return c.getInt(columnIdx);
    }


    private UtilsCursor() {}
}
