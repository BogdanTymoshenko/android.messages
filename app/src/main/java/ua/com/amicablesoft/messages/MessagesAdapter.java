package ua.com.amicablesoft.messages;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.com.amicablesoft.messages.model.ChatMessage;
import ua.com.amicablesoft.messages.utils.UtilsDateFormat;
import ua.com.amicablesoft.messages.utils.UtilsHashTag;

/**
* Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/16/14.
*/
public class MessagesAdapter extends CursorAdapter {

    private static final int VIEW_TYPE__MESSAGE_FROM_ME = 0;
    private static final int VIEW_TYPE__MESSAGE_FROM_CONTACT = 1;


    public MessagesAdapter(Context context) {
        super(context, null, false);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View messageView;
        switch (getItemViewType(cursor)) {
            case VIEW_TYPE__MESSAGE_FROM_ME:
                messageView = LayoutInflater.from(context).inflate(R.layout.list_item_message_from_me, parent, false);
                break;
            case VIEW_TYPE__MESSAGE_FROM_CONTACT:
                messageView = LayoutInflater.from(context).inflate(R.layout.list_item_message_from_contact, parent, false);
                break;
            default:
                throw new IllegalStateException("Unknown view type");
        }

        messageView.setTag(new ViewHolder(messageView));

        return messageView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ChatMessage message = ChatMessage.fromCursor(cursor);

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.messageText.setText(UtilsHashTag.highlightHashTags(message.getText()));
        holder.messageDate.setText(UtilsDateFormat.formatMessageDate(message.getDate()));
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        return getItemViewType(cursor);
    }

    public String getMessageText(View itemView) {
        if (itemView != null) {
            if (itemView.getTag() instanceof ViewHolder) {
                ViewHolder holder = (ViewHolder) itemView.getTag();
                return holder.messageText.getText().toString();
            }
        }

        return null;
    }


    private int getItemViewType(Cursor cursor) {
        ChatMessage.Type messageType = ChatMessage.fromCursor(cursor).getType();
        switch (messageType) {
            case FROM_ME:
                return VIEW_TYPE__MESSAGE_FROM_ME;
            case FROM_CONTACT:
                return VIEW_TYPE__MESSAGE_FROM_CONTACT;
            default:
                throw new IllegalStateException("Unknown message type "+messageType);
        }
    }



    private static final class ViewHolder {
        public final TextView messageText;
        public final TextView messageDate;

        private ViewHolder(View itemView) {
            messageText = (TextView) itemView.findViewById(R.id.list_item_message__message_text);
            messageDate = (TextView) itemView.findViewById(R.id.list_item_message__message_date);
        }
    }
}
