package ua.com.amicablesoft.messages;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import ua.com.amicablesoft.messages.model.ChatMessage;
import ua.com.amicablesoft.messages.service.ChatService;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
public class MessagesActivity extends ActionBarActivity {

    private static final int LOADER_ID__MESSAGES_LOADER = 100;

    private int MESSAGE__MAX_LENGTH;


    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            chatService = ((ChatService.ServiceBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            chatService = null;
        }
    };


    private MessagesAdapter messagesAdapter;
    private ChatService chatService;

    private ListView messagesList;
    private EditText messageField;

    private boolean needScrollDown;
    private boolean needDestroyService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        MESSAGE__MAX_LENGTH = getResources().getInteger(R.integer.message__max_length);

        Intent startCharService = new Intent(this, ChatService.class);
        startService(startCharService);

        messagesAdapter = new MessagesAdapter(this);

        messagesList = (ListView) findViewById(R.id.activity_messages__messages_list);
        messagesList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        messagesList.setAdapter(messagesAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            messagesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View itemView, int position, long id) {
                    String messageText = messagesAdapter.getMessageText(itemView);
                    if (!TextUtils.isEmpty(messageText)) {
                        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("message text", messageText);
                        clipboardManager.setPrimaryClip(clip);
                        Toast.makeText(MessagesActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }

        messageField = (EditText) findViewById(R.id.activity_messages__message_field);
        messageField.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null &&
                        (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_SEND)) {
                    sendMessage();
                    return true;
                }

                return false;
            }
        });

        ImageButton sendButton = (ImageButton) findViewById(R.id.activity_messages__send_message_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        getSupportLoaderManager().initLoader(LOADER_ID__MESSAGES_LOADER, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
                return new MessagesLoader(MessagesActivity.this);
            }

            @Override
            public void onLoadFinished(Loader<Cursor> objectLoader, Cursor cursor) {
                messagesAdapter.changeCursor(cursor);

                if (needScrollDown) {
                    int lastVisiblePosition = messagesList.getLastVisiblePosition();
                    int lastMessagePosition = messagesAdapter.getCount() - 1;
                    if (lastVisiblePosition < lastMessagePosition) {
                        messagesList.setSelection(lastMessagePosition);
                    }
                    needScrollDown = false;
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> objectLoader) {
                // do nothing
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindChatService = new Intent(this, ChatService.class);
        bindService(bindChatService, serviceConnection, 0);
    }

    @Override
    protected void onStop() {
        if (chatService != null) {
            unbindService(serviceConnection);
            chatService = null;
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (needDestroyService) {
            Intent stopChatService = new Intent(this, ChatService.class);
            stopService(stopChatService);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        needDestroyService = true;
    }

    private void sendMessage() {
        String message = messageField.getText().toString();
        message = message.trim();
        message = (message.length() > MESSAGE__MAX_LENGTH) ? message.substring(0, MESSAGE__MAX_LENGTH) : message;
        if (!message.isEmpty() && chatService != null) {
            chatService.sendMessage(new ChatMessage(message, ChatMessage.Type.FROM_ME));
            messageField.setText(null);

            needScrollDown = true;
        }
    }
}
