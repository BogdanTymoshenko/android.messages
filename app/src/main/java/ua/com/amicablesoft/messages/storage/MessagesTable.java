package ua.com.amicablesoft.messages.storage;

import android.provider.BaseColumns;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
public final class MessagesTable {

    public static final String TABLE_NAME = "messages";
    public static final String COLUMN_ID = BaseColumns._ID;
    public static final String COLUMN_MESSAGE_TEXT = "message_text";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TYPE = "type";

    public static final String CREATE_TABLE_SCRIPT =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID+          " INTEGER   PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                    COLUMN_MESSAGE_TEXT+" TEXT      NOT NULL, "+
                    COLUMN_DATE+        " INTEGER   NOT NULL, "+
                    COLUMN_TYPE+        " INTEGER   NOT NULL);";


    private MessagesTable() {
    }
}
