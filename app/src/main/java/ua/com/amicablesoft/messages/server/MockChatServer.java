package ua.com.amicablesoft.messages.server;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ua.com.amicablesoft.messages.model.ChatMessage;

/**
* Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
*/
public class MockChatServer {

    private static final String TAG = MockChatServer.class.getSimpleName();

    private static final String MESSAGES[] = {
            "What is your favorite #candy?",
            "Where do you #work?",
            "What is your morning routine?",
            "What's the worst thing about your gender?"
    };

    private final Random random = new Random(47);

    private final Object lock = new Object();

    private List<ChatMessage> generatedMessages = new LinkedList<ChatMessage>();

    private Thread messageGenerationThread;
    private volatile boolean isConnected;


    public void connect() {
        Log.d(TAG, "Connect to mock chat server");
        disconnect();

        isConnected = true;
        messageGenerationThread = new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "Start message generation");
                while (isConnected) {
                    boolean canGenerateMessage = random.nextBoolean();
                    if (canGenerateMessage) {
                        String nextMessage = MESSAGES[random.nextInt(MESSAGES.length)];
                        Log.d(TAG, "Generated message: "+nextMessage);

                        synchronized (lock) {
                            generatedMessages.add(new ChatMessage(nextMessage, ChatMessage.Type.FROM_CONTACT));
                        }
                    }

                    try {
                        Thread.sleep(5000L);
                    }
                    catch (InterruptedException ignored) {}
                }
                Log.d(TAG, "Message generation completed");
            }
        });
        messageGenerationThread.start();
        Log.d(TAG, "Done");
    }

    public void disconnect() {
        if (isConnected && messageGenerationThread != null) {
            Log.d(TAG, "Disconnect from mock chat server");
            isConnected = false;
            try {
                messageGenerationThread.join();
            } catch (InterruptedException ignored) {
            }

            messageGenerationThread = null;
            Log.d(TAG, "Done");
        }
    }


    public List<ChatMessage> retrieveMessages() {
        synchronized (lock) {
            List<ChatMessage> newMessages = new ArrayList<ChatMessage>(generatedMessages.size());
            newMessages.addAll(generatedMessages);
            generatedMessages.clear();

            return newMessages;
        }
    }
}
