package ua.com.amicablesoft.messages.utils;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/17/14.
 */
public final class UtilsHashTag {

    private static final Pattern PATTERN__HASH_TAG = Pattern.compile("(#[A-Za-z0-9_-]+)");


    public static CharSequence highlightHashTags(String inputString) {
        SpannableStringBuilder outputString = new SpannableStringBuilder(inputString);

        Matcher matcher = PATTERN__HASH_TAG.matcher(inputString);

        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();

            outputString.setSpan(new ForegroundColorSpan(Color.RED), start, end, 0);
        }
        return outputString;
    }

    private UtilsHashTag() {}
}
