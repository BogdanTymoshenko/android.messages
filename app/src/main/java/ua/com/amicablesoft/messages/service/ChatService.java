package ua.com.amicablesoft.messages.service;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ua.com.amicablesoft.messages.model.ChatMessage;
import ua.com.amicablesoft.messages.server.MockChatServer;
import ua.com.amicablesoft.messages.storage.MessageStorage;

public class ChatService extends Service {

    private static final long MESSAGE_RETRIEVE__INITIAL_DELAY__SEC = 1;
    private static final long MESSAGE_RETRIEVE__DELAY__SEC = 3;

    private final IBinder binder = new ServiceBinder();
    private final MockChatServer chatServer = new MockChatServer();

    private Handler uiThreadHander;
    private ScheduledExecutorService messageRetrieveExecutor;
    private MessageStorage storage;

    private MessagesUpdateListener listener;


    public class ServiceBinder extends Binder {
        public ChatService getService() {
            return ChatService.this;
        }
    }


    public ChatService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();

        uiThreadHander = new Handler(Looper.getMainLooper());
        storage = new MessageStorage(getApplicationContext());

        messageRetrieveExecutor = Executors.newSingleThreadScheduledExecutor();
        messageRetrieveExecutor.execute(new Runnable() {
            @Override
            public void run() {
                chatServer.connect();
            }
        });
        messageRetrieveExecutor.scheduleWithFixedDelay(
                new Runnable() {
                    @Override
                    public void run() {
                        List<ChatMessage> newMessages = chatServer.retrieveMessages();
                        if (!newMessages.isEmpty()) {
                            storage.saveMessages(newMessages);
                            notifyNewMessagesArrived();
                        }
                    }
                },
                MESSAGE_RETRIEVE__INITIAL_DELAY__SEC,
                MESSAGE_RETRIEVE__DELAY__SEC,
                TimeUnit.SECONDS);
    }


    private void notifyNewMessagesArrived() {
        uiThreadHander.post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.onMessagesUpdated();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (messageRetrieveExecutor != null) {
            messageRetrieveExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    chatServer.disconnect();
                }
            });
            messageRetrieveExecutor.shutdown();
            messageRetrieveExecutor = null;
        }

        storage.close();
        storage = null;

        uiThreadHander = null;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }



    public void sendMessage(ChatMessage message) {
        if (message != null) {
            storage.saveMessage(message);
            notifyNewMessagesArrived();
        }
    }

    public Cursor findMessages() {
        return storage.findMessages();
    }


    public void setMessagesUpdateListener(MessagesUpdateListener listener) {
        this.listener = listener;
    }

}
