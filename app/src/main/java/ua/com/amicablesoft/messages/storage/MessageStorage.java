package ua.com.amicablesoft.messages.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import ua.com.amicablesoft.messages.model.ChatMessage;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
public class MessageStorage {

    private final MessageStorageHelper messageStorageHelper;
    private final SQLiteDatabase db;


    public MessageStorage(Context context) {
        messageStorageHelper = new MessageStorageHelper(context.getApplicationContext());
        db = messageStorageHelper.getWritableDatabase();
    }


    public void close() {
        db.close();
        messageStorageHelper.close();
    }


    public void saveMessages(List<ChatMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return;
        }

        for (ChatMessage message : messages) {
            saveMessage(message);
        }
    }

    public void saveMessage(ChatMessage message) {
        if (message == null) {
            return;
        }

        ContentValues insertValues = new ContentValues();
        insertValues.put(MessagesTable.COLUMN_DATE, message.getDate().getMillis());
        insertValues.put(MessagesTable.COLUMN_MESSAGE_TEXT, message.getText());
        insertValues.put(MessagesTable.COLUMN_TYPE, message.getType().value);
        db.insert(MessagesTable.TABLE_NAME, null, insertValues);
    }

    public Cursor findMessages() {
        return db.query(
                MessagesTable.TABLE_NAME,
                new String[]{
                        "*"
                },
                null, /* where */
                null, /* where args */
                null, /* group by */
                null, /* having */
                MessagesTable.COLUMN_DATE
        );
    }
}
