package ua.com.amicablesoft.messages;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.IBinder;
import android.support.v4.content.AsyncTaskLoader;

import ua.com.amicablesoft.messages.service.ChatService;
import ua.com.amicablesoft.messages.service.MessagesUpdateListener;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com on 11/15/14.
 */
public class MessagesLoader extends AsyncTaskLoader<Cursor> implements MessagesUpdateListener {

    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            chatService = ((ChatService.ServiceBinder)service).getService();
            chatService.setMessagesUpdateListener(MessagesLoader.this);
            forceLoad();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            cancelLoad();
            chatService.setMessagesUpdateListener(null);
            chatService = null;
        }
    };

    private ChatService chatService;


    public MessagesLoader(Context context) {
        super(context);

        Intent bindChatService = new Intent(context.getApplicationContext(), ChatService.class);
        context.getApplicationContext().bindService(bindChatService, serviceConnection, 0);
    }


    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        if (takeContentChanged()) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        if (chatService != null) {
            chatService.setMessagesUpdateListener(null);
            getContext().getApplicationContext().unbindService(serviceConnection);
            chatService = null;
        }
    }


    @Override
    public Cursor loadInBackground() {
        if (chatService != null) {
            return chatService.findMessages();
        }

        return null;
    }


    @Override
    public void onMessagesUpdated() {
        onContentChanged();
    }
}
