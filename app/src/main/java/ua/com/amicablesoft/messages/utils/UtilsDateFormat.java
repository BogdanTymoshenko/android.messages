package ua.com.amicablesoft.messages.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/16/14.
 */
public final class UtilsDateFormat {

    private static final DateTimeFormatter FORMATTER__TIME = DateTimeFormat.forPattern("HH:mm");
    private static final DateTimeFormatter FORMATTER__MONTH_DAY_TIME = DateTimeFormat.forPattern("MMM d, HH:mm");


    public static String formatMessageDate(DateTime dateTime) {
        DateTimeFormatter formatter = selectFormatter(dateTime);
        return formatter.print(dateTime);
    }



    private static DateTimeFormatter selectFormatter(DateTime dateTime) {
        DateTime now = new DateTime(DateTimeZone.getDefault());

        if (Days.daysBetween(now.withTimeAtStartOfDay(), dateTime.withTimeAtStartOfDay()).getDays() == 0) {
            return FORMATTER__TIME;
        }
        else {
            return FORMATTER__MONTH_DAY_TIME;
        }
    }

    private UtilsDateFormat() {}
}
