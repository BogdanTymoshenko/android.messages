package ua.com.amicablesoft.messages.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bogdan Tymoshenko <bogdan.tymoshenko@gmail.com> on 11/15/14.
 */
/*package*/ class MessageStorageHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "messages.sqlite";
    private static final int DATABASE_VERSION = 1;


    public MessageStorageHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MessagesTable.CREATE_TABLE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // do nothing
    }
}
